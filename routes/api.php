<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['prefix' => 'v1'], function (){
    Route::group(['namespace' => 'Auth'], function (){
        Route::post('register', 'RegisterController@create');
        Route::post('login', 'RegisterController@store');

    });

    Route::group(['namespace' => 'Post'], function (){
        Route::get('posts', 'PostController@index');
        Route::get('posts/{post}', 'PostController@show');
        Route::get('comments', 'CommentController@index');
        Route::get('posts/{post}/comments', 'CommentController@indexCommentsPost');
    });


    Route::group(['middleware' => 'auth:api'], function (){

        Route::group(['namespace' => 'Auth'], function (){
            Route::get('logout', 'RegisterController@logout');
        });

        Route::group(['namespace' => 'Post'], function (){
            Route::post('posts', 'PostController@store');
            Route::post('posts/{post}', 'PostController@update');
            Route::delete('posts/{post}', 'PostController@delete');
            Route::get('tags', 'TagController@index');
            Route::post('tags', 'TagController@store');
            Route::delete('tags/{tag}', 'TagController@destroy');
            Route::post('comments', 'CommentController@store');
            Route::delete('comments/{comment}', 'CommentController@destroy');

        });

        Route::group(['namespace' => 'User'], function (){
            Route::get('users', 'DetailController@index');
            Route::get('users/{user}', 'DetailController@show');
            Route::put('users/{user}', 'DetailController@update');
            Route::post('users/update-mobile/send-code', 'MobileController@create');
            Route::post('users/update-mobile', 'MobileController@store');
        });

    });

});
