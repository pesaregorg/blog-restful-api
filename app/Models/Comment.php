<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    const STATUS_ACCEPTED = 200;
    const STATUS_REJECTED = 103;
    const STATUS_PENDING = 100;


    protected $table = 'comments';
    protected $fillable = [
        'post_id', 'name', 'mobile', 'body', 'status', 'reply_id'
    ];

    protected $hidden = [
      'mobile'
    ];

    protected $attributes = [
        'status' => self::STATUS_PENDING
    ];


    public function scopeIsAccepted($query)
    {
        return $query->where([['status', self::STATUS_ACCEPTED],['reply_id',null]]);
    }

    public function reply($id)
    {
        return self::where('reply_id',$id);
    }
}
