<?php

namespace App\Models;

use App\Traits\ResponseJson;
use App\User;
use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{
    use ResponseJson;

    protected $table = 'sms_codes';

    protected $fillable = [
        'mobile', 'code'
    ];

    protected $hidden = [
        'code'
    ];


    public function create()
    {
        $code = random_int(1000, 9999);
        $smsCode = $this->firstOrCreate(['mobile' => toEn($this->mobile)]);
        $smsCode->code = $code;
        $smsCode->save();
        // send code sms

        if ($smsCode){
            return $this->responseJsonApi($smsCode, $this::$STATUS_RESPONSE_OK);
        }
        return $this->responseJsonApi($smsCode, 304);
    }


    public function scopeVerify($query, $array)
    {
        return $query->where([['mobile','=' ,toEn($array['mobile'])], ['code' , '=', toEn($array['code'])]])->count();

    }


}
