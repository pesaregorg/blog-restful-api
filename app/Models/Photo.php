<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class Photo extends Model
{
    protected $table = 'photos';
    protected $fillable = [
        'user_id', 'parent_id', 'category', 'photo'
    ];


    public function saveStoragePhoto($image, $type)
    {
        $imageClientName = $image->getClientOriginalName();
        $newName = Str::random().'_'.Carbon::now()->milliseconds.'_'.$imageClientName;
        $smallName = 'small_'.$newName;
        $mediumName = 'medium_'.$newName;
        $originalName = 'original_'.$newName;

        $smallSize = Image::make($image)->widen(100)->stream();
        $meduimSize = Image::make($image)->widen(300)->stream();
        $orginalSize = Image::make($image)->stream();
        Storage::disk('project')->put($type. '/' . $smallName, $smallSize);
        Storage::disk('project')->put($type. '/' . $mediumName, $meduimSize);
        Storage::disk('project')->put($type. '/' . $originalName, $orginalSize);

        return $newName;

    }

    public function store()
    {
        return self::create($this->attributes);
    }

    public function blogFileDelete()
    {
        $path = 'blog/';
        Storage::disk('project')->delete([
            $path . 'small_' . $this->photo,
            $path . 'medium_' . $this->photo,
            $path . 'original_' . $this->photo
        ]);
        return $this->delete();
    }
}
