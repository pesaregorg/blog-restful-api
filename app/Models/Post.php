<?php

namespace App\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    use Sluggable;


    const TYPE_POST = 'post';
    const TYPE_PAGE = 'page';


    protected $table = 'posts';
    protected $fillable = [
        'user_id', 'title', 'body', 'type', 'slug', 'url'
    ];




    public static function shortUrl($url)
    {
        return strtolower(preg_replace('/[^a-z0-9]+/i', '-', $url));
    }

    public function postTag()
    {
        return $this->hasMany(PostTag::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }


    public function photo()
    {
        return $this->hasMany(Photo::class, 'parent_id','id')->where('category', 'post');
    }

    public function comment()
    {
        return $this->hasMany(Comment::class);
    }




}
