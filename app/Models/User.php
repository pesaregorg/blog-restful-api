<?php

namespace App\Models;

use App\Models\OauthAccessToken;
use App\Models\SmsCode;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;



    const TYPE_ADMIN = 'admin';
    const TYPE_USER = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','mobile', 'email', 'type', 'subscribed_at', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $attributes = [
        'type' => self::TYPE_USER
    ];

    public function signUp($params)
    {
        $smsCode = SmsCode::verify($params);

        if ($smsCode != 1){
            return ['status' => false, 'message' => 'کد تایید اشتباه است'];
        }

        $user = self::firstOrCreate([
            'mobile' => toEn($params['mobile'])
        ]);
        auth()->login($user);
        $token = $user->createToken('tokenAccess')->accessToken;
        return ['status' => true, 'access_token' => $token];

   }

    public function AauthAcessToken(){
        return $this->hasMany(OauthAccessToken::class);
    }

    public function scopeIsAdmin()
    {
        if ($this->type == self::TYPE_ADMIN){
            return true;
        }
        return false;
    }


    public function post()
    {
        return $this->hasMany(Post::class);
    }


}
