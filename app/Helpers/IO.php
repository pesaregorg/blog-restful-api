<?php
function toFa($string){
    $numberEn = range(0, 9);
    $numberFa = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    return str_replace($numberEn , $numberFa , $string);
}

function toEn($string){
    $numberEn = range(0, 9);
    $numberFa = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    return str_replace($numberFa, $numberEn, $string);
}


