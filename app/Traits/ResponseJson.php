<?php
namespace App\Traits;

trait ResponseJson {

    public static $STATUS_RESPONSE_OK = 200;
    public static $STATUS_RESPONSE_CREATED = 201;
    public static $STATUS_RESPONSE_ACCEPTED = 202;
    public static $STATUS_RESPONSE_NOT_CONTENT = 204;
    public static $STATUS_RESPONSE_MOVED_PERMANENTLY = 301;
    public static $STATUS_RESPONSE_FOUND = 302;
    public static $STATUS_RESPONSE_SEE_OTHER = 303;
    public static $STATUS_RESPONSE_NOT_MODIFIED = 304;
    public static $STATUS_RESPONSE_TEMPORARY_REDIRECT = 307;
    public static $STATUS_RESPONSE_BAD_REQUEST = 400;
    public static $STATUS_RESPONSE_UNAUTHORIZED = 401;
    public static $STATUS_RESPONSE_FORBIDDEN = 403;
    public static $STATUS_RESPONSE_NOT_FOUND = 404;
    public static $STATUS_RESPONSE_METHOD_NOT_ALLOWED = 405;
    public static $STATUS_RESPONSE_METHOD_NOT_ACCEPTABLE = 406;
    public static $STATUS_RESPONSE_METHOD_PRECONDITION_FAILED = 412;
    public static $STATUS_RESPONSE_METHOD_UNSUPPORTED_MEDIA_TYPE = 415;
    public static $STATUS_RESPONSE_METHOD_INTERNET_SERVER_ERROR = 500;
    public static $STATUS_RESPONSE_METHOD_NOT_IMPLEMENTED = 501;

    public function responseJsonApi($data, $statusCode)
    {

        return response()->json($data, $statusCode);
    }
}
