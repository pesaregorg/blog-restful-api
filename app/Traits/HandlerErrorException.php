<?php
namespace App\Traits;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Illuminate\Validation\ValidationException;

trait HandlerErrorException{

    use ResponseJson;
    public function responseException(Exception $exception)
    {

        switch (true){
            case $exception instanceof ModelNotFoundException:
                $retval = response()->json(['message' => 'Resource not found!'] , $exception->status);
                break;
            case $exception instanceof AuthenticationException:
                $retval = response()->json(['message' => 'unAuthorized!'] , $exception->status);
                break;
            case $exception instanceof ValidationException:
                $retval = response()->json($exception->errors() , $exception->status);
                break;
            case $exception instanceof ServerException:
                $retval = response()->json(['message' => 'Server Error'] , $exception->status);
                break;
            case $exception instanceof RequestException:
                $retval = response()->json(['message' => 'Request Error'] , $exception->status);
                break;

            default:
                $retval = response()->json($exception->getMessage() , $this::$STATUS_RESPONSE_BAD_REQUEST);
                break;
        }

        return $retval;
    }

}
