<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentForm;
use App\Http\Resources\CommentCollection;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        return response()->json(Comment::latest()->paginate(), 200);
    }

    public function indexCommentsPost(Post $post)
    {
        $comments = $post->comment()->isAccepted()->get();
        return response()->json(new CommentCollection($comments), 200);
    }

    public function store(CommentForm $form)
    {
        $comment = Comment::create($form->all());
        return response()->json($comment, 201);
    }

    
    public function destroy(Comment $comment)
    {
        $user = auth()->user();
        if (!$user->isAdmin()){
            return response()->json('شما به این متد دسترسی ندارید', 400);
        }
        return response()->json($comment->delete(), 200);
    }
}
