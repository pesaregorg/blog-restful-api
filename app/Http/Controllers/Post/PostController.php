<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostForm;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Photo;
use App\Models\Post;
use App\Models\PostTag;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        $posts = new PostCollection(Post::latest()->paginate());
        return response()->json($posts, 200);
    }

    public function store(PostForm $form)
    {

        $user = auth()->user();
        $this->authorize('create', Post::class);
        $post = $user->post()->create(array_filter($form->all()));
        $post->url = Post::shortUrl($form->url);
        $post->save();

        if (array_key_exists('photo', $form->all())){
            $uploadPhoto = new Photo();
            $photo = $uploadPhoto->saveStoragePhoto($form->photo, 'blog');
            $array = [
                'user_id' => $user->id,
                'parent_id' => $post->id,
                'category' => Post::TYPE_POST,
                'photo' => $photo
            ];
            $storePhoto = new Photo($array);
            $storePhoto->store();
        }

        if (array_key_exists('tags', $form->all())){
            foreach ($form->tags as $tag){
                PostTag::create([
                    'post_id' => $post->id,
                    'tag_id' => $tag
                ]);
            }
        }

        return response()->json(new PostResource($post), 201);
    }



    public function update(PostForm $form, Post $post)
    {
        $this->authorize('update', $post);
        $postUpdated = $post->update(array_filter($form->all()));

        // check photo
        if (array_key_exists('photo', $form->all())){
            // check old photo
            $oldPhoto = $post->photo()->first();
            if ($oldPhoto){
                $oldPhoto->blogFileDelete();
            }
            // add new photo
            $uploadPhoto = new Photo();
            $photo = $uploadPhoto->saveStoragePhoto($form->photo, 'blog');
            $array = [
                'user_id' => auth()->user()->id,
                'parent_id' => $post->id,
                'category' => Post::TYPE_POST,
                'photo' => $photo
            ];
            $storePhoto = new Photo($array);
            $storePhoto->store();
        }

        // check tags
        if (array_key_exists('tags', $form->all())){

            // check old tags
            $tags = $post->postTag()->get();
            if ($tags){
                foreach ($tags as $tag){
                    $tag->delete();
                }
            }
            //add new tags
            foreach ($form->tags as $tag){
                PostTag::create([
                    'post_id' => $post->id,
                    'tag_id' => $tag
                ]);
            }
        }

        return response()->json($postUpdated, 201);
    }

    public function delete(Post $post)
    {
        $this->authorize('delete', $post);

        $photo = $post->photo()->first();
        if ($photo){
            $photo->blogFileDelete();
        }

        $tags = $post->postTag()->get();
        if ($tags){
            foreach ($tags as $tag){
                $tag->delete();
            }
        }

        return response()->json($post->delete(), 200);
    }

    public function show(Post $post)
    {
        return response()->json(new PostResource($post), 200);
    }
}
