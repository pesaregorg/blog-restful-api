<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\TagForm;
use App\Models\Tag;
use Illuminate\Http\Request;


class TagController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return response()->json(['data' => $tags], 200);
    }

    public function store(TagForm $form)
    {
        $this->authorize('create', Tag::class);
        $tag = new Tag();
        $tag->user_id = auth()->user()->id;
        $tag->tag = $form->tag;
        $tag->save();
        return response()->json($tag, 201);
    }

    public function destroy(Tag $tag){
        $this->authorize('delete', $tag);
        return response()->json($tag->delete(), 200);
    }
}
