<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\MobileForm;
use App\Http\Requests\Auth\VerificationSmsForm;
use App\Models\SmsCode;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{

    protected function create(MobileForm $form)
    {
        $signUp =  $form->persist();
        return $signUp;
    }

    public function store(VerificationSmsForm $form)
    {
        $signUp = $form->persist();
        if ($signUp['status']){
            return response()->json($signUp, 201);
        }
        return response()->json($signUp, 404);
    }


    public function logout()
    {
        if (Auth::check()) {
            $user = Auth::user()->token();
            return response()->json($user->revoke(), 200);
        }
        return response()->json(false, 200);

    }

    public function logoutAllDevices()
    {
        if (Auth::check()) {
            $logout = Auth::user()->AauthAcessToken()->delete();
            return response()->json($logout, 200);
        }
        return response()->json(false, 200);

    }
}
