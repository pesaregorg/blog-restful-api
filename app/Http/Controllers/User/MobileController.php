<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\MobileForm;
use App\Http\Requests\Auth\VerificationSmsForm;

class MobileController extends Controller
{
    protected function create(MobileForm $form)
    {
        $sendCode =  $form->persist();
        return $sendCode;
    }

    public function store(VerificationSmsForm $form)
    {
        return $form->updateMobile();
    }
}
