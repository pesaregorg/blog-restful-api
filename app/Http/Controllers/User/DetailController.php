<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\DetailForm;
use App\Models\User;
use Illuminate\Http\Request;

class DetailController extends Controller
{

    public function index()
    {
        $user = auth()->user();
        $this->authorize('view', $user);
        return response()->json(User::latest()->paginate(), 200);

    }

    public function show(User $user)
    {
        $this->authorize('view', $user);
        return response()->json($user, 200);
    }

    public function update(DetailForm $form, User $user)
    {
        $this->authorize('update', $user);

        return response()->json($user->update(array_filter($form->all())), 200);
    }
}
