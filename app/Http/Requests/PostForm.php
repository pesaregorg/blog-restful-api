<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;


class PostForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'min:8'],
            'body' => ['required'],
            'type' => ['required'],
            'short_url' => ['min:8'],
            'tags' => ['array'],
            'photo' =>  ['mimes:jpeg,jpg,png' , 'max:10000']
        ];
    }


}
