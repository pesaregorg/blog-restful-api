<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => ['required', 'max:191'],
            'mobile'    => ['required', 'size:11'],
            'body'      => ['required', 'max:400'],
            'post_id'   => ['required', 'numeric', 'max:20'],
            'reply_id'  => ['numeric', 'max:20']
        ];
    }
}
