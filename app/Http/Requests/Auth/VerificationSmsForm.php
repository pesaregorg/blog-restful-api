<?php

namespace App\Http\Requests\Auth;

use App\Models\SmsCode;
use App\Traits\ResponseJson;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class VerificationSmsForm extends FormRequest
{
    use ResponseJson;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "mobile" => ['required' , 'size:11'],
            "code" => ['required', 'size:4']
        ];
    }

    public function persist()
    {
        $user = new User();
        return $user->signUp($this->all());
    }

    public function updateMobile()
    {
        $user = auth()->user();
        $smsCode = SmsCode::verify($this->all());

        if ($smsCode != 1){
            return response()->json(['status' => false, 'message' => 'کد تایید اشتباه است'], 401);
        }
        $update = $user->update(['mobile' => toEn($this->mobile)]);
        return response()->json($update, 200);
    }

    public function adsMobileVerify()
    {
        $user = auth()->user();
        $smsCode = SmsCode::verify($this->all());

        if ($smsCode != 1){
            return response()->json(['status' => false, 'message' => 'کد تایید اشتباه است'], 401);
        }

        $mobile = $user->adsMobileVerify()->create([
            'verified_at' => Carbon::now(),
            'mobile' => $this->mobile
        ]);

        return response()->json($mobile, 200);
    }

}
