<?php

namespace App\Http\Requests\Auth;

use App\Models\SmsCode;
use Illuminate\Foundation\Http\FormRequest;

class MobileForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => ['required', 'size:11'],
        ];
    }

    public function persist()
    {
        $code = new SmsCode($this->all());
        $response = $code->create();
        return $response;
    }
}
