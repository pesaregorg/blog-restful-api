<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'body' => mb_strimwidth($this->body ,0,25,'...'),
            'type' => $this->type,
            'tags' => PostTagResource::collection($this->postTag()->get()),
            'photo' => $this->photo()->first()
        ];
    }
}
