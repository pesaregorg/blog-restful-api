<?php

namespace App\Http\Middleware;

use App\Traits\ResponseJson;
use Closure;

class ApiToken
{
    use ResponseJson;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('X-API-TOKEN') !== env('X_API_TOKEN')){
            return $this->responseJsonApi(['message' => 'unAuthorized!'],
                $this::$STATUS_RESPONSE_UNAUTHORIZED);
        }
        return $next($request);
    }
}
