<?php

namespace App\Http\Middleware;

use Closure;

class CheckPaymentCallback
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->Authorization) && isset($request->app_token)){
            $request->headers->set('Accept', 'application/json');
            $request->headers->set('Content-Type', 'application/json');
            $request->headers->set('X-API-TOKEN', $request->app_token);
            $request->headers->set('Authorization', $request->Authorization);
        }
        return $next($request);
    }
}
